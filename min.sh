#!/bin/bash

ABBREV='min'
OWNER='rubyonracetracks'
DISTRO='debian'
SUITE='stretch'
DOCKER_IMAGE="$OWNER/$DISTRO-$SUITE-$ABBREV"
CONTAINER="container-$DISTRO-$SUITE-$ABBREV"

bash setup.sh $ABBREV $DOCKER_IMAGE $CONTAINER
